OBJ = main
SRC = $(OBJ).tex
PDF = $(OBJ).pdf
TEXC := xelatex
TEXC_OPTS += -shell-escape

.PHONY: all clean

all: $(PDF)

$(PDF): $(SRC)
	$(TEXC) $(TEXC_OPTS) $(SRC)
	$(TEXC) $(TEXC_OPTS) $(SRC)

clean:
	@git clean -xfd
	rm -f $(PDF)
